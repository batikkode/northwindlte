<?php
  foreach ($dataProduct as $product) {
    ?>
    <tr>
      <td style="min-width:230px;"><?php echo $product->ProductID; ?></td>
      <td><?php echo $product->ProductName; ?></td>
      <td><?php echo $product->QuantityPerUnit; ?></td>
      <td><?php echo $product->UnitPrice; ?></td>
      <td><?php echo $product->UnitsInStock; ?></td>
      <td class="text-center" style="min-width:230px;">
        <button class="btn btn-warning update-dataProduct" data-id="<?php echo $product->ProductID; ?>"><i class="glyphicon glyphicon-repeat"></i> Update</button>
        <button class="btn btn-danger konfirmasiHapus-product" data-id="<?php echo$product->ProductID; ?>" data-toggle="modal" data-target="#konfirmasiHapus"><i class="glyphicon glyphicon-remove-sign"></i> Delete</button>
      </td>
    </tr>
    <?php
  }
?>