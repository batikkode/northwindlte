<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Add Data Product</h3>

  <form id="form-tambah-product" method="POST">
    <div class="input-group form-group">
      <span class="input-group-addon" >
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Product Name" name="ProductName" aria-describedby="sizing-addon2">
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" >
        <i class="glyphicon glyphicon-home"></i>
      </span>
      <select name="SupplierID" id="SupplierID" class="form-control" style="width: 100%" aria-describedby="sizing-addon2">        
        <?php
        foreach ($dataSupplier as $supplier) {
          ?>
          <option value="<?php echo $supplier->SupplierID; ?>">
            <?php echo $supplier->ContactName; ?>
          </option>
          <?php
        }
        ?>
      </select>
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" >
        <i class="glyphicon glyphicon-home"></i>
      </span>
      <select name="CategoryID" id="CategoryID" class="form-control" style="width: 100%" aria-describedby="sizing-addon2">        
        <?php
        foreach ($dataCategory as $category) {
          ?>
          <option value="<?php echo $category->CategoryID; ?>">
            <?php echo $category->CategoryName; ?>
          </option>
          <?php
        }
        ?>
      </select>
    </div>
  
    <div class="input-group form-group">
      <span class="input-group-addon" >
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Quantity PerUnit" name="QuantityPerUnit" aria-describedby="sizing-addon2">
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" >
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Unit Price" name="UnitPrice" aria-describedby="sizing-addon2">
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" >
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Units In Stock" name="UnitsInStock" aria-describedby="sizing-addon2">
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" >
        <i class="glyphicon glyphicon-user"></i>
      </span>
      <input type="text" class="form-control" placeholder="Units On Order" name="UnitsOnOrder" aria-describedby="sizing-addon2">
    </div>
    <div class="form-group">
      <div class="col-md-12">
          <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i>Submit Data</button>
      </div>
    </div>
  </form>
</div>


<script type="text/javascript">
$(function () {
    $("#SupplierID").select2();    
    $("#CategoryID").select2();    
});
</script>

