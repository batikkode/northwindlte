<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

	public function select_all_product() {		
		$data = $this->db->get('products');
		return $data->result();
	}

	public function select_all() {
		$data = $this->db->get('products');
		return $data->result();
	}

	public function select_all_supplier() {
		$data = $this->db->get('suppliers');
		return $data->result();
	}

	public function select_by_id($id) {
		$this->db->where('ProductID',$id);
		$data = $this->db->get('products');
		return $data->row();
	}

	public function select_by_category($id) {
		$sql = "SELECT COUNT(*) AS jml FROM products WHERE CategoryID = {$id}";
		$data = $this->db->query($sql);
		return $data->row();
	}

	public function select_by_supplier($id) {
		$sql = "SELECT COUNT(*) AS jml FROM suppliers WHERE SupplierID = {$id}";
		$data = $this->db->query($sql);
		return $data->row();
	}

	public function update($data) {
		$list = array(
			'ProductName' => $data['ProductName'],
			'SupplierID' => $data['SupplierID'],
			'CategoryID' => $data['CategoryID'],
			'QuantityPerUnit' => $data['QuantityPerUnit'],
			'UnitPrice' => $data['UnitPrice'],
			'UnitsInStock' => $data['UnitsInStock'],
			'UnitsOnOrder' => $data['UnitsOnOrder'],
			'LastUpdate' => now()
		);
		$this->db->where('ProductID',$data['ProductID']);
		$this->db->update('products', $list);				
		return $this->db->affected_rows();
	}

	public function delete($id) {
		$this->db->where('ProductID', $id);
		$this->db->delete('products');
		return $this->db->affected_rows();
	}

	public function insert($data) {
		$list = array(
			'ProductName' => $data['ProductName'],
			'SupplierID' => $data['SupplierID'],
			'CategoryID' => $data['CategoryID'],
			'QuantityPerUnit' => $data['QuantityPerUnit'],
			'UnitPrice' => $data['UnitPrice'],
			'UnitsInStock' => $data['UnitsInStock'],
			'UnitsOnOrder' => $data['ProductName'],
			'LastUpdate' => now()
		);
		$this->db->insert('products',$list);
		return $this->db->affected_rows();
	}

	public function insert_batch($data) {
		$this->db->insert_batch('products', $data);		
		return $this->db->affected_rows();
	}

	public function check_nama($nama) {
		$this->db->where('ProductName', $nama);
		$data = $this->db->get('products');
		return $data->num_rows();
	}

	public function total_rows() {
		$data = $this->db->get('products');
		return $data->num_rows();
	}
}

/* End of file M_pegawai.php */
/* Location: ./application/models/M_pegawai.php */