<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Product_model','PM');		
		$this->load->model('Category_model','CM');
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['dataProduct'] = $this->PM->select_all();
		$data['dataCategory'] = $this->CM->select_all();
		$data['dataSupplier'] = $this->PM->select_all_supplier();
		$data['page'] = "product";
		$data['judul'] = "Data Product";
		$data['deskripsi'] = "Manage Data Product";
		$data['modal_tambah_product'] = show_my_modal('modals/modal_tambah_product', 'tambah-product', $data);
		$this->template->views('product/home', $data);
	}

	public function tampil() {
		$data['dataProduct'] = $this->PM->select_all();
		$this->load->view('product/list_data', $data);
	}
	
	public function prosesTambah() {
		$this->form_validation->set_rules('ProductName', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('SupplierID', 'Supplier', 'trim|required');
		$this->form_validation->set_rules('CategoryID', 'Categories', 'trim|required');
		$this->form_validation->set_rules('QuantityPerUnit', 'Quantity', 'trim|required');
		$this->form_validation->set_rules('UnitPrice', 'Unit', 'trim|required');
		$this->form_validation->set_rules('UnitsInStock', 'Stock', 'trim|required');
		$this->form_validation->set_rules('UnitsOnOrder', 'Order', 'trim|required');

		$data = $this->input->post();
		if ($this->form_validation->run() == TRUE) {
			$result = $this->PM->insert($data);

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Product Successfully Added', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Product Failed Added', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function update() {
		$id = trim($_POST['id']);

		$data['dataProduct'] = $this->PM->select_by_id($id);
		$data['dataCategory'] = $this->CM->select_all();
		$data['dataSupplier'] = $this->PM->select_all_supplier();
		$data['userdata'] = $this->userdata;

		echo show_my_modal('modals/modal_update_product', 'update-product', $data);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('ProductName', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('SupplierID', 'Supplier', 'trim|required');
		$this->form_validation->set_rules('CategoryID', 'Categories', 'trim|required');
		$this->form_validation->set_rules('QuantityPerUnit', 'Quantity', 'trim|required');
		$this->form_validation->set_rules('UnitPrice', 'Unit', 'trim|required');
		$this->form_validation->set_rules('UnitsInStock', 'Stock', 'trim|required');
		$this->form_validation->set_rules('UnitsOnOrder', 'Order', 'trim|required');

		$data = $this->input->post();
		if ($this->form_validation->run() == TRUE) {
			$result = $this->PM->update($data);

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Product Successfully Updated', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Product Failed Updated', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function delete() {
		$id = $_POST['id'];
		$result = $this->PM->delete($id);
		if ($result > 0) {
			echo show_succ_msg('Data Product Successfully Delete', '20px');
		} else {
			echo show_err_msg('Data Product Failed Delete', '20px');
		}
	}

	public function export() {
		// lihat contoh yang di controller category
	}

	public function import() {
		// lihat contoh yang di controller category
	}
}

/* End of file Pegawai.php */
/* Location: ./application/controllers/Pegawai.php */