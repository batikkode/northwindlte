<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Category extends AUTH_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Category_model','CM');
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$data['dataCategory'] 	= $this->CM->select_all();

		$data['page'] 		= "category";
		$data['judul'] 		= "Data Category ";
		$data['deskripsi'] 	= "Manage Data Category";

		$data['modal_tambah_category'] = show_my_modal('modals/modal_tambah_category', 'tambah-category', $data);

		$this->template->views('category/home', $data);
	}

	public function tampil() {
		$data['dataCategory'] = $this->CM->select_all();
		$this->load->view('category/list_data', $data);
	}

	public function prosesTambah() {
		$this->form_validation->set_rules('categoryname', 'Category', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');
		$data 	= $this->input->post();
		
		if ($this->form_validation->run() == TRUE) {
			$config['upload_path'] = './assets/img/';
			$config['allowed_types'] = 'jpg|png';
			
			$this->load->library('upload', $config);
			
			if (!$this->upload->do_upload('picture')){
				$error = array('error' => $this->upload->display_errors());
			}
			else{
				$data_foto = $this->upload->data();
				$data['Picture'] = $data_foto['file_name'];				
			}
			$result = $this->CM->insert($data);

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Category Successfully Added', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data category Failed Added', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function update() {
		$data['userdata'] 	= $this->userdata;
		$id 				= trim($_POST['id']);
		$data['dataCategory'] 	= $this->CM->select_by_id($id);

		echo show_my_modal('modals/modal_update_category', 'update-category', $data);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('categoryname', 'Category', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');
		$data 	= $this->input->post();
		if ($this->form_validation->run() == TRUE) {
			$result = $this->CM->update($data);

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Category Successfully  Update', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data category Failed Update', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}
		echo json_encode($out);
	}

	public function delete() {
		$id = $_POST['id'];
		$result = $this->CM->delete($id);
		
		if ($result > 0) {
			echo show_succ_msg('Data Category Successfully Delete', '20px');
		} else {
			echo show_err_msg('Data Category Successfully Failed', '20px');
		}
	}

	public function detail() {
		$data['userdata'] 	= $this->userdata;

		$id 				= trim($_POST['id']);
		$data['category'] = $this->CM->select_by_id($id);
		$data['jumlahCategory'] = $this->CM->total_rows();

		echo show_my_modal('modals/modal_detail_category', 'detail-category', $data, 'lg');
	}

	public function export() {
		$spreadsheet = new Spreadsheet();
		$activeWorksheet = $spreadsheet->getActiveSheet();
		$activeWorksheet->setCellValue('A1', 'ID');
		$activeWorksheet->setCellValue('B1', 'Category Name');
		$activeWorksheet->setCellValue('C1', 'Description');
		$data = $this->CM->select_all();
		$rowCount = 2;
		foreach($data as $value){
		    $activeWorksheet->setCellValue('A'.$rowCount, $value->CategoryID); 
		    $activeWorksheet->setCellValue('B'.$rowCount, $value->CategoryName); 
			$activeWorksheet->setCellValue('C'.$rowCount, $value->Description); 
		    $rowCount++; 
		} 

		$writer = new Xlsx($spreadsheet);
		$writer->save('./assets/excel/data_category.xlsx');
		redirect(base_url().'category');
	}

	public function import() {
		$this->form_validation->set_rules('excel', 'File', 'trim|required');

		if ($_FILES['excel']['name'] == '') {
			$this->session->set_flashdata('msg', 'File harus diisi');
		} else {
			$config['upload_path'] = './assets/excel/';
			$config['allowed_types'] = 'xls|xlsx';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('excel')){
				$error = array('error' => $this->upload->display_errors());
			}
			else{
				$data = $this->upload->data();
				
				error_reporting(E_ALL);
				date_default_timezone_set('Asia/Jakarta');

				$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

				$inputFileName = './assets/excel/' .$data['file_name'];
				$spreadsheet = $reader->load($inputFileName);
        		$sheetData = $spreadsheet->getActiveSheet()->toArray();
			
				$index = 0;
				for($i=1;$i<count($sheetData);$i++) {					
					$check = $this->CM->check_nama($sheetData[$i][1]);
					if ($check != 1) {
						$resultData[$index]['CategoryID'] = $sheetData[$i][0];
						$resultData[$index]['CategoryName'] = $sheetData[$i][1];
						$resultData[$index]['Description'] = $sheetData[$i][2];
					}					
					$index++;
				}

				unlink('./assets/excel/' .$data['file_name']);

				if (count($resultData) != 0) {
					$result = $this->CM->insert_batch($resultData);
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Data Category Successfully Import to database'));
						redirect('category');
					}
				} else {
					$this->session->set_flashdata('msg', show_msg('Data Category failed import to database (Already update)', 'warning', 'fa-warning'));
					redirect('category');
				}

			}
		}
	}
}

/* End of file Kota.php */
/* Location: ./application/controllers/Kota.php */